package com.book.application.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.Data;

@Entity
@Data
@Cache (usage=CacheConcurrencyStrategy.TRANSACTIONAL)
public class Book {

	@Id
	private Integer id;
	private String title;
	private String category;
	private String isbn;
	private Integer numberOfPages;
	private String year;
	private String author;
	private String publisher;
	private Date releaseDate;
	private Double price;

}
