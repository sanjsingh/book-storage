package com.book.application.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.book.application.service.BookService;

@RestController
@RequestMapping("book")
public class BookController {
	

	@Autowired
	private BookService service;
	
	@GetMapping
	public String test() {
		return "Test"+service;
		
		
	}
	
	@Autowired
    ResourceBundleMessageSource messageSource;   

    @GetMapping("/{locale}/getMessage")
    public String getLocaleMessage(@PathVariable("locale") Locale locale) {
        return messageSource.getMessage("message.name",null,locale);
	
	
    }
	

}
